﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared.Models
{
    public class MessageDto
    {
        public int MsgId { get; set; }
        public string MsgSub { get; set; }
        public DateTime SentOn { get; set; }
        public string MsgBody { get; set; }
        
    }
}
