﻿using System;

namespace MicroserviceB.Models
{
    public class Service2Dto
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
    }
}
