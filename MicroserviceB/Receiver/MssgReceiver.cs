﻿using MassTransit;
using MicroserviceB.DAL;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Shared.Models;
using System;
using System.Threading.Tasks;

namespace MicroserviceB.Receiver
{
    public class MssgReceiver :IConsumer<MessageDto>
    {
        private readonly Service2DBContext _dbContext;
       
        private readonly ILogger<MssgReceiver> _logger;
        public MssgReceiver(Service2DBContext dbContext , ILogger<MssgReceiver> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }
        public async Task Consume(ConsumeContext<MessageDto> context)
        {
            try
            {
                var data = context.Message;
                var rec = new Models.Service2Dto { Message = JsonConvert.SerializeObject(data) };
                await _dbContext.Service2Dto.AddAsync(rec);
                await _dbContext.SaveChangesAsync();

                await context.Publish(data);

                await context.RespondAsync<MessageDto>(new
                {
                   Value = $"Received: {context.Message.MsgId}"
                });
               
            }
            catch (Exception ex)
            {
                _logger.LogError("ConsumerError", ex);
            }

        }

       
    }
}
