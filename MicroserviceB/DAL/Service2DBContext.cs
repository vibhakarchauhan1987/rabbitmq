﻿using MicroserviceB.Models;
using Microsoft.EntityFrameworkCore;

namespace MicroserviceB.DAL
{
    public class Service2DBContext: DbContext
    {
        public Service2DBContext(DbContextOptions<Service2DBContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Service2Dto>().ToTable("Service2");
        }

        public DbSet<Service2Dto> Service2Dto { get; set; }
    }
}
