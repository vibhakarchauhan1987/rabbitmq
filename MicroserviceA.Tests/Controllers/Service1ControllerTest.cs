﻿using MassTransit;
using MicroserviceA.Controllers;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Moq;
using MicroserviceA.DAL;
using Shared.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace MicroserviceA.Tests.Controllers
{
    public class Service1ControllerTest
    {
        [Fact]
        public void CreateMessage_ShouldReturnSuccess_WhenValidRequest()
        {
            //Arrange
            var bus = new Mock<IBus>();
            var dbContext = new Mock<Service1DBContext>();
            Service1Controller controller = new Service1Controller(bus.Object, dbContext.Object);
            var requestDto = new MessageDto
            {
                MsgBody = "Test",
                MsgSub = "Test",
                SentOn = DateTime.Now,
            };
            //Act
            var res = controller.CreateMessage(requestDto);
            //Assert
            Assert.IsType<Task<IActionResult>>(res);
        }

        [Fact]
        public void CreateMessage_ShouldReturnBadRequest_WhenInvalidRequest()
        {
            //Arrange
            var bus = new Mock<IBus>();
            var dbContext = new Mock<Service1DBContext>();
            Service1Controller controller = new Service1Controller(bus.Object, dbContext.Object);

            //Act
            var res = controller.CreateMessage(null);

            //Assert
            //Assert.IsType<Task<BadRequestResult>>(res);
            Assert.IsType<Task<IActionResult>>(res);
        }
    }
}