﻿using MassTransit;
using MicroserviceA.DAL;
using Newtonsoft.Json;
using Shared.Models;
using System;
using System.Threading.Tasks;

namespace MicroserviceA.Acknowledge
{
    public class Ackrcvr : IConsumer<MessageDto>
    {
        private readonly Service1DBContext _dbContext;
        private readonly IBus _bus;
        public Ackrcvr(Service1DBContext dbContext, IBus bus)
        {
            _dbContext = dbContext;
            _bus = bus;
        }
        public async Task Consume(ConsumeContext<MessageDto> context)
        {
            try
            {
                var data = context.Message;

                await _dbContext.Service1Dto.AddAsync(new Models.Service1Dto { Message = JsonConvert.SerializeObject(data) });
                await _dbContext.SaveChangesAsync();
                Uri uri = new Uri("amqp://rabbitmq:5672/ackQueue");
                var endPoint = await _bus.GetSendEndpoint(uri);
                await endPoint.Send(true);
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine($"{ex.ToString()}");
                throw;
            }

        }
    
    }
}
