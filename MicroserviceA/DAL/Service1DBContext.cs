﻿using MicroserviceA.Models;
using Microsoft.EntityFrameworkCore;

namespace MicroserviceA.DAL
{
    public class Service1DBContext : DbContext
    {
        public Service1DBContext()
        {

        }
        public Service1DBContext(DbContextOptions<Service1DBContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Service1Dto>().ToTable("Service1");
        }

        public DbSet<Service1Dto> Service1Dto { get; set; }
    }
}
