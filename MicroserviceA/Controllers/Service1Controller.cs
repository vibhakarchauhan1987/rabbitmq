﻿using MassTransit;
using MicroserviceA.DAL;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Shared.Models;
using System;
using System.Threading.Tasks;

namespace MicroserviceA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Service1Controller : ControllerBase
    {
        private readonly IBus _bus;
        private readonly Service1DBContext _dbContext;

        public Service1Controller(IBus bus, Service1DBContext dbContext)
        {
            _bus = bus;
            _dbContext = dbContext;
        }
        [HttpPost]
        public async Task<IActionResult> CreateMessage(MessageDto message)
        {
            try
            {
                if (message != null)
                {
                    var req = new Models.Service1Dto { Message = JsonConvert.SerializeObject(message)};
                    message.SentOn = DateTime.Now;
                    await _dbContext.Service1Dto.AddAsync(req);
                    await _dbContext.SaveChangesAsync();
                    Uri uri = new Uri("amqp://rabbitmq:5672/ticketQueue");
                    var endPoint = await _bus.GetSendEndpoint(uri);
                    await endPoint.Send(message);
                    return Ok();
                }
                return BadRequest();
            }
            catch
            {
                return StatusCode(500);
            }
        }
    }
}