﻿using System;

namespace MicroserviceA.Models
{
    public class Service1Dto
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDate { get; set; }=DateTime.Now;
        //public bool Status { get; set; }
        ////0-Pending
        ////1- Success
    }
}
